package main

import ("fmt"
	"flag"
	"time"
	"math/rand"
)

func update_field(x int, y int, playfield[2] [][]uint8, activeField int){
	for ri := 0; ri < y; ri++ {
	for ci := 0; ci < x; ci++ {
		//count neighbors
		var nb int;
		//ylä vas
		if playfield[activeField][(ri-1+x)%x][(ci-1+y)%y] > 0 { nb++ }//ylä vas
		if playfield[activeField][(ri-1+x)%x][ ci       ] > 0 { nb++ }//ylä kes
		if playfield[activeField][(ri-1+x)%x][(ci+1+y)%y] > 0 { nb++ }//ylä oik
		if playfield[activeField][ ri       ][(ci-1+y)%y] > 0 { nb++ }//kes vas
		if playfield[activeField][ ri       ][(ci+1+y)%y] > 0 { nb++ }//kes oik
		if playfield[activeField][(ri+1+x)%x][(ci-1+y)%y] > 0 { nb++ }//ala vas
		if playfield[activeField][(ri+1+x)%x][ ci       ] > 0 { nb++ }//ala kes
		if playfield[activeField][(ri+1+x)%x][(ci+1+y)%y] > 0 { nb++ }//ala oik

		//determen next field state
		if nb  > 3{ playfield[(activeField+1)%2][ri][ci] = 0 }
		if nb  < 2{ playfield[(activeField+1)%2][ri][ci] = 0 }
		if nb == 2{ playfield[(activeField+1)%2][ri][ci] = playfield[activeField][ri][ci]}
		if nb == 3{ playfield[(activeField+1)%2][ri][ci] = 1 }
	}}
}
func drawfield(x int, y int, playfield[2] [][]uint8, activeField int){
	fmt.Printf("\033[1;5H");
	for ri := 0; ri < y; ri++ {
	for ci := 0; ci < x; ci++ {
		if playfield[activeField][ri][ci] == 1 { fmt.Printf("██") }
		if playfield[activeField][ri][ci] == 0 { fmt.Printf("  ") }
	}
		fmt.Printf("\n");
	}
}

func main() {
	x := flag.Int("x", 16, "x dimension")
	y := flag.Int("y", 16, "y dimension")
	flag.Parse()
	var playfield[2] [][]uint8

	//seed rng
	rand.Seed(time.Now().UnixNano());

	//fill and "allocate" slices
	for i := 0; i < 2; i++{
		playfield[i] = make([][]uint8, *y)
		for ri := 0; ri < *y; ri++ {
			playfield[i][ri] = make([]uint8, *x)
			for ci := 0; ci < *x; ci++ {
				playfield[i][ri][ci] = uint8(rand.Int() % 2);
			}
		}
	}


	//mainloop
	for giter := 0; giter < 100000; giter++ {
		update_field(*x, *y, playfield, giter%2);
		drawfield(*x, *y, playfield, giter%2);
		time.Sleep(500 * time.Millisecond);
	}
}