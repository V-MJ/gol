#include <stdio.h>	//printing
#include <stdlib.h>	//used for random intagers
#include <stdint.h>	//uin8_t
#include <time.h>	//used to seed the rng
#include <unistd.h>	//sleep

//global variables
uint8_t** boards[2];		//buffes to store the game state
signed int SX = 16;		//x dimension
signed int SY = 16;		//y dimension
signed int SS = 1;		//how long to wait
signed int SEED;		//random by default current unix time
signed int closeCells = 0;	//used to determen cells right to live


void cellCheck(int row, int column, uint8_t** board){
	if (board[row][column])
		closeCells++;
}

int main(int argc, char *argv[]){

	//argument handeling
	SEED = time(0); //hopefully compiler can optimize this out
	for(int i = 1; i < argc; i++){
		switch(argv[i][1]){
			case 'r': SEED = atoi(argv[i+1]); i++; break;
			case 'x': SX   = atoi(argv[i+1]); i++; break;
			case 'y': SY   = atoi(argv[i+1]); i++; break;
			case 's': SS   = atoi(argv[i+1]); i++; break;
			default:
				fprintf(stderr, "Usage: %s [-rxys]\n", argv[0]);
				exit(EXIT_FAILURE);
		}
	}
	srand(SEED);	//set random seed

	//allocate boards
	boards[0] = malloc(sizeof(uint8_t*)*SX);
	for (int i = 0; i<SX; i++){
		boards[0][i] = malloc(sizeof(uint8_t*)*SY); 
	}
	boards[1] = malloc(sizeof(uint8_t*)*SX);
	for (int i = 0; i<SX; i++){
		boards[1][i] = malloc(sizeof(uint8_t*)*SY); 
	}
	

	//remore cursor, "clear" terminal
	printf("\e[?25l");
	printf("\e[1;1H\e[2J");
	
	//fill board with random intagers
	for (int y = 0; y < SY; y++){
	for (int x = 0; x < SX; x++){
		boards[0][x][y] = rand() % 2;
	}}

	int boardSelect = 0;
	//graphics and logic loop
	while(1){
		
		//render the array
		printf("\033[0;0H");//move cursor to the zeroth row and zeroth column
		for (int y = 0; y < SY; y++){
		for (int x = 0; x < SX; x++){
			//2 chars are printed as that makes them look more lice squares
			switch (boards[boardSelect][x][y]){
				case 1:
					printf("██");
					break;
				default :
					printf("  ");
			}
		}
			printf("\n");
		}
		

		//the loop of life
		//I tried to make the playfield loop on it's self. So top and bottom of the playfield are connected. Same for left and right.
		for(int y = 0; y < SY; y++){
		for(int x = 0; x < SX; x++){
			//neighbour checks
			
			//corners
			cellCheck((x - 1 + SX) % SX, (y - 1 + SY) % SY, boards[boardSelect]);//top left
			cellCheck((x - 1 + SX) % SX, (y + 1 + SY) % SY, boards[boardSelect]);//top right
			cellCheck((x + 1) % SX, (y - 1 + SY) % SY, boards[boardSelect]);//bottom left
			cellCheck((x + 1) % SX, (y + 1 + SY) % SY, boards[boardSelect]);//bottom right
			//middle row
			cellCheck((x - 1 + SX) % SX, y, boards[boardSelect]);//top middle
			cellCheck((x + 1 + SX) % SX, y, boards[boardSelect]);//bottom midle
			//middle column
			cellCheck(x, (y - 1 + SY) % SY, boards[boardSelect]);//left
			cellCheck(x, (y + 1 + SY) % SY, boards[boardSelect]);//right

			//check if cell should be alive
			if(closeCells < 2)	{ boards[(boardSelect+1)%2][x][y] = 0; }			//underpopulation death
			if(closeCells == 2)	{ boards[(boardSelect+1)%2][x][y] = boards[boardSelect][x][y]; }//same state as previously
			if(closeCells > 3)	{ boards[(boardSelect+1)%2][x][y] = 0; }			//overpopulation death
			if(closeCells == 3)	{ boards[(boardSelect+1)%2][x][y] = 1; }			//cell is born)
			//reset closeCells for next loop
			closeCells = 0;
		}}

		//make the program wait
		sleep(SS);

		//switch the active board
		boardSelect = (boardSelect + 1 ) % 2;
	}

}